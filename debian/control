Source: libemail-find-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libemail-valid-perl,
                     libemail-address-perl
Standards-Version: 3.9.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libemail-find-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libemail-find-perl.git
Homepage: https://metacpan.org/module/Email::Find

Package: libemail-find-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libemail-valid-perl,
         libemail-address-perl
Description: module to find RFC 822 email addresses in plain text
 Email::Find is a module for finding a subset of RFC 822 email
 addresses in arbitrary text.  The addresses it
 finds are not guaranteed to exist or even actually be email addresses
 at all, but they will be valid RFC 822 syntax.
 .
 Email::Find will perform some heuristics to avoid some of the more
 obvious red herrings and false addresses, but there's only so much
 which can be done without a human.
